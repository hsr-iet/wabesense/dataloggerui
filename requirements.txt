numpy
scipy
pandas
matplotlib
pyaml
pyserial

# html gui
flask

# documentation
Sphinx

# User utils
ipython

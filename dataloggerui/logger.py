"""
 Logger basic configuration and other miscellaneous definitions
"""
import datetime
import re
import string

from pathlib import Path
from collections import namedtuple

import yaml
from flask import Flask, flash, render_template, request, url_for, redirect
from werkzeug.utils import secure_filename

channel_idx = dict(
    adc_in8_name=0,
    adc_in9_name=1,
    adc_in10_name=2,
    adc_in11_name=3
)
""" Mapping from internal channel name to channel index """

datetime_fmt_rtc_set = '%Y.%m.%d %u %H:%M:%S'
datetime_fmt_rtc_get = '%Y.%m.%d %u %H:%M:%S.%f'
""" RTC datetime format string 

    See https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior
    for information about the format string.
"""

datetime_fmt_sample = '%Y.%m.%d %H:%M:%S.%f'
""" Datetime format of logger sample """

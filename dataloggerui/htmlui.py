"""
 Example data logger configuration

 Test using
 ::

    FLASK_APP=htmlui.py python -m flask run

"""
import datetime
import re
import string

from pathlib import Path
from collections import namedtuple
import logging

import yaml
from flask import Flask, flash, render_template, request, url_for, redirect
from werkzeug.utils import secure_filename

import pandas as pd

try:
    from . import serialui as sui
    from .logger import channel_idx
except ImportError:
    import serialui as sui
    from logger import channel_idx

_logfile = (Path.cwd() / Path(__file__).name).with_suffix('.log')
logging.basicConfig(filename=f'{_logfile.name}',
                    level=logging.DEBUG, filemode="w",
                    format='%(asctime)s %(levelname)s:%(name)s:%(message)s',
                    datefmt='%d.%m.%Y %H:%M:%S %Z')

datetime_fmt = '%a %b %d %H:%M:%S.%f %Y %Z'
""" Date and Time format on the UI """

defaults = dict(
    sampling_interval=10,
    interval_units='T',
    auto_sampleStatus=True,
    battery_cell_voltage=3.7,
    battery_cell_count=3,
    adc_in8_name='ADC_IN8(V)',
    adc_in9_name='ADC_IN9(V)',
    adc_in10_name='ADC_IN10(V)',
    adc_in11_name='ADC_IN11(V)',
)
""" Default values """

SetButtonPress = namedtuple('SetButtonPress',
                            ['logging_set_btn',
                             'logging_clear_data_btn',
                             'devname_set_btn',
                             'chname_set_btn',
                             'battery_set_btn'], defaults=[False]*4)
""" Flags indicating what button SET buttons where pressed """

current_values_empty = dict(
    device_name='',
    sampling_interval='',
    interval_units='T',
    auto_sampleStatus=False,
    samples_count='',
    battery_cell_voltage='',
    battery_cell_count='',
    firmware_version='',
    unique_id='',
    adc_in8_name='',
    adc_in9_name='',
    adc_in10_name='',
    adc_in11_name='',
    rtc_now='',
    serial_port='',
    no_logger='',
)
current_values = current_values_empty.copy()
current_values.update(defaults)

config_file_tmpl = string.Template(
'''Device name: $device_name
Time based measurement: $auto_sampleStatus
Measurement interval (minutes): $sampling_interval
Battery nominal voltage (mV): $battery_cell_voltage
Battery number of cells: $battery_cell_count
Analog channel mappings:
  ADC_IN8 (HW): $adc_in8_name
  ADC_IN9 (HW): $adc_in9_name
  ADC_IN10 (HW): $adc_in10_name
  ADC_IN11 (HW): $adc_in11_name
Firmware version: $firmware_version
Unique ID: $unique_id
''')


config_file_dict = {u[0].strip(): u[-1].strip()
                    for line in config_file_tmpl.template.replace(': $', ';').split('\n')[:-1]
                    if not (u := line.partition(';'))[0].startswith('Analog')}
""" Mapping from configuration file as printed by firmware to HTML forms"""


def load_config_file(fname):
    """ Load data from configuration file as printed by firmware

        Parameters
        ----------
        :class:`pathlib.Path` or :class:`str`
            Path to file

        Returns
        -------
        :class:`dict`
            The keys are the parameters used in the html forms
    """

    with open(fname, 'r') as f:
        filedict = yaml.load(f, Loader=yaml.FullLoader)
        filedict.update(filedict.pop('Analog channel mappings'))
        formdict = {config_file_dict[k]: v for k, v in filedict.items()
                    if k in config_file_dict}

    # Make sure device name is str and conserve unique id
    for field in ('device_name', 'unique_id'):
        if isinstance(formdict[field], int):
            hexfield = f"{formdict[field]:#026X}"
            formdict[field] = hexfield.replace('X', 'x', 1)

    # mV to V
    formdict['battery_cell_voltage'] = float(formdict['battery_cell_voltage']) * 1e-3

    # 'activated'/'deactivated' to boolean
    formdict['auto_sampleStatus'] = formdict['auto_sampleStatus'] == 'activated'

    # measurement interval units
    formdict['interval_units'] = 'T'

    return formdict


def save_config_file(fname, data):
    """ Save form data to configuration file as printed by firmware

        Parameters
        ----------
        fname: :class:`pathlib.Path` or :class:`str`
            Path to file
        data: :class:`dict`
            The keys are the parameters used in the html forms
    """
    # V to mV
    data['battery_cell_voltage'] = int(float(data['battery_cell_voltage']) * 1e3)

    # 'enabled/disabled' to 'activated'/'deactivated'
    d_ = {'enabled': 'activated', 'disabled': 'deactivated'}
    data['auto_sampleStatus'] = d_[data['auto_sampleStatus']]

    # empty channel names to 'NA'
    for k, v in data.items():
        if k.startswith('adc_'):
            if not v:
                data[k] = 'NA'

    # Fill config template
    file_content = config_file_tmpl.safe_substitute(**data)
    fname.write_text(file_content)


def get_config_logger():
    """ Get all information from the logger.
    """
    # Connect to logger
    if not sui.wbsLogger.is_open:
        sui.wbsLogger.open()

    # get configuration
    config_dict = sui.get_configuration()
    config_dict['sampling_interval'] = str(config_dict['sampling_interval'])
    config_dict['rtc_now'] = config_dict['rtc_now'].strftime(datetime_fmt)
    return config_dict


def utcnow():
    """ TZ aware current date time"""
    return datetime.datetime.now(datetime.timezone.utc).strftime(datetime_fmt)


def get_download_folder(home=None):
    """ Return or create configurations download folder """
    home = Path().home() if home is None else home
    folder = home / 'datalogger_configuration'
    if not folder.exists():
        folder.mkdir()
    return folder


app = Flask(__name__)
app.secret_key = b'_5#y2L"Q4Q8z\n\xec]/'
app.config['UPLOAD_FOLDER'] = str(get_download_folder(home=None))


def get_download_link(devname):
    """ Generate download location from device name.
    """
    if devname:
        fname = devname + '.yml'
        fname = (Path(app.config['UPLOAD_FOLDER']) / secure_filename(fname)).resolve()
        return str(fname)
    else:
        return ''


def render_configui(**kwargs):
    logging.info('Rendering configuration page')
    logging.debug(kwargs)
    return render_template('logger_ui_config.html', **kwargs)


def goto_config(**kwargs):
    return redirect(url_for('html_ui_get', **kwargs))


def connection_lost():
    sui.wbsLogger.close()
    sui.wbsLogger.port = ''
    flash('Connection to logger lost!')
    return redirect(url_for('logger_connect'))


def goto_connect():
    global current_values
    # Restart tabula rasa
    sui.wbsLogger.close()
    current_values = current_values_empty.copy()
    return redirect(url_for('logger_connect'))


working_without_logger = False
""" Whether user choose to work without a logger """


@app.route('/logger_ui', methods=['GET', 'POST'])
def logger_connect():
    global working_without_logger

    # Clear all: tabula rasa
    sui.disconnect_logger()

    ports = sui.list_logger_ports()
    
    if len(ports) == 0:
        ports = sui.list_ports()
        flash("Couldn't find connected logger")

    if request.method == 'POST':
        try:
            sui.connect_logger(port=request.form['serial_port'])
        except sui.serial.SerialException as e:
            flash(f'{e}')
            return render_template('logger_ui_connect.html', ports=ports)

        working_without_logger = False
        return redirect(url_for('html_ui_get', reload_btn=''))
    else:
        if 'nologger' in request.args:
            # Continue without logger
            working_without_logger = True
            return redirect(url_for('html_ui_get', clear_btn=''))

        return render_template('logger_ui_connect.html', ports=ports)


@app.route('/logger_ui/config', methods=['POST'])
def html_ui_post():
    global current_values

    # Update current values with form data
    logging.info("Updating current values with data from html form")
    logging.debug(current_values)
    current_values.update({k: v for k, v in request.form.items()
                           if k in current_values})
    logging.debug(current_values)

    # Check if logger still present
    if not sui.logger_present() and (not working_without_logger):
        return connection_lost()

    # FORM: Loading from file
    if 'load_btn' in request.form:
        # check if the post request has the file part
        if 'load_file' not in request.files:
            flash('Load: missing file')
            return render_configui(**current_values)
        file = request.files['load_file']
        if file.filename:
            filename = secure_filename(file.filename)
            try:
                fname = Path(app.config['UPLOAD_FOLDER']) / filename
                file.save(fname)
                filedict = load_config_file(fname)
            except FileNotFoundError as e:
                flash(f'{e}')
                return render_configui(**current_values)
            # Merge with current values
            current_values.update(filedict)
            current_values['download_link'] = str(fname)
        else:
            flash('Load: No file name')
        return render_configui(**current_values)

    # FORM: Date and time
    if 'pc_clock_btn' in request.form:
        # get PC date time
        logging.info('Getting PC datetime')
        logging.debug(current_values['rtc_now'])
        current_values['rtc_now'] = utcnow()
        logging.debug(current_values['rtc_now'])
        return render_configui(**current_values)

    # FORM: All configuration forms
    # Attend to RTC sets first
    if 'clock_sync_btn' in request.form:
        logging.info('Clock sync button pressed')
        try:
            sui.sync_rtc()
            logging.info('Synchronization complete')
        except (RuntimeError, ValueError) as e:
            flash(f'{e}')
        logging.debug(current_values['rtc_now'])
        current_values['rtc_now'] = sui.get_datetime().strftime(datetime_fmt)
        logging.debug(current_values['rtc_now'])
        return render_configui(**current_values)

    if 'clock_set_btn' in request.form:
        dt = datetime.datetime.strptime(request.form['rtc_now'],
                                        datetime_fmt)
        try:
            sui.set_datetime(dt)
        except (RuntimeError, ValueError) as e:
            flash(f'{e}')
        sui.store_configuration()  # setting the clock is persistent
        current_values['rtc_now'] = sui.get_datetime().strftime(datetime_fmt)
        return render_configui(**current_values)

    if 'logger_clock_btn' in request.form:
        # get logger date time
        logging.info("Get Logger datetime")
        logging.debug(current_values['rtc_now'])
        current_values['rtc_now'] = sui.get_datetime().strftime(datetime_fmt)
        logging.debug(current_values['rtc_now'])
        return render_configui(**current_values)

    # All these require serial port
    if 'serial_port' in request.form:
        if request.form['serial_port']:
            # Check if logger still present
            if not sui.logger_present():
                return connection_lost()

            # SET buttons
            pressed = SetButtonPress(*[f in request.form
                                       for f in SetButtonPress._fields])
            if ('upload_btn' in request.form) or ('setall_btn' in request.form):
                pressed = SetButtonPress(logging_set_btn=True,
                                         logging_clear_data_btn=False,  # Do not erase data
                                         devname_set_btn=True,
                                         chname_set_btn=True,
                                         battery_set_btn=True)

            # Set buttons
            if pressed.logging_set_btn:
                try:
                    sui.set_sampling_interval(int(request.form['sampling_interval']))
                    interval, units = sui.get_sampling_interval()
                    current_values['sampling_interval'] = str(interval)
                    current_values['interval_units'] = units

                    sui.set_auto_sampling(request.form['auto_sampling'] == '1')
                    current_values['auto_sampleStatus'] = sui.get_auto_sampling()
                    
                    current_values['samples_count'] = sui.get_sample_count()
                except (RuntimeError, ValueError) as e:
                    flash(f'{e}')

            if pressed.logging_clear_data_btn:
                try:
                    sui.delete_samples()
                    
                    sui.set_auto_sampling(request.form['auto_sampling'] == '1')
                    current_values['auto_sampleStatus'] = sui.get_auto_sampling()
                    
                    current_values['samples_count'] = sui.get_sample_count()
                except (RuntimeError, ValueError) as e:
                    flash(f'{e}')

            if pressed.devname_set_btn:
                try:
                    sui.set_device_name(request.form['device_name'])
                    current_values['device_name'] = sui.get_device_name()
                except (RuntimeError, ValueError) as e:
                    flash(f'{e}')

            if pressed.chname_set_btn:
                for ch, idx in channel_idx.items():
                    try:
                        sui.set_analog_channel_name(channel=idx,
                                                    name=request.form[ch])
                        current_values[ch] = sui.get_analog_channel_name(channel=idx)
                    except (RuntimeError, ValueError) as e:
                        flash(f'{e}')

            if pressed.battery_set_btn:
                V = float(request.form['battery_cell_voltage'])
                cc = int(request.form['battery_cell_count'])
                try:
                    sui.set_battery_configuration(cell_voltage=V, cell_count=cc)
                    V_, cc_ = sui.get_battery_configuration()
                    current_values['battery_cell_voltage'] = str(V_)
                    current_values['battery_cell_count'] = str(cc_)
                except (RuntimeError, ValueError) as e:
                    flash(f'{e}')

            if 'upload_btn' in request.form:
                sui.store_configuration()

    # Download link always points to a predefined folder in user home
    current_values['download_link'] = get_download_link(current_values['device_name'])

    # Saving to file
    if 'save2file_btn' in request.form:
        saved = False
        if current_values['download_link']:
            fname = Path(current_values['download_link'])
            save_config_file(fname, current_values)
            saved = True
        else:
            flash("Can't save without Device Name")
        return render_configui(saved=saved, **current_values)

    return render_configui(**current_values)


@app.route('/logger_ui/config', methods=['GET'])
def html_ui_get():
    global current_values

    if 'buildnames_btn' in request.args:
        return redirect(url_for('html_ui_namebuilder'))

    if 'connect_btn' in request.args:
        return goto_connect()

    if 'clear_btn' in request.args:
        port_ = current_values['serial_port']
        current_values = current_values_empty.copy()
        current_values['serial_port'] = port_
        return render_configui(**current_values)

    if 'default_btn' in request.args:
        # Merge with current values
        for k, v in defaults.items():
            if not current_values[k] or current_values[k] == 'NA':
                current_values[k] = v
        if not current_values['rtc_now']:
            current_values['rtc_now'] = utcnow()
        current_values['download_link'] = get_download_link(current_values['device_name'])
        return render_configui(**current_values)

    if not sui.logger_present() and (not working_without_logger):
        return connection_lost()

    if not working_without_logger:
        if 'reload_btn' in request.args:
            current_values = get_config_logger()

        if 'samples_btn' in request.args:
            return redirect(url_for('html_ui_samples', get_sample_btn=''))

    # Download link always points to a predefined folder in user home
    current_values['download_link'] = get_download_link(current_values['device_name'])

    return render_configui(**current_values)


site_information = dict(
    location_name='',
    location_building='',
    wabe_id='',
    wabe_type='',
    wabe_spring='',
    sensor_name='',
    pressure_ch='',
    temp_ch='',
)
""" Site information dictionary."""


def sanitize_name(name, *, reverse=False):
    """ Convert string to accepted format.
    """
    rep = (' ', '_')
    rep = rep if not reverse else rep[::-1]
    return name.replace(*rep).strip()


date_re_ = r'(?P<year>\d{4})(?P<month>\d{2})(?P<day>\d{2})'

devname_re = re.compile(r'(?P<location_name>[\w_-]+)[.]'
                        r'(?P<location_building>\w{2,3})[.]'
                        r'(?P<wabe_spring>[\w_\d-]+)$')

senname_re = re.compile(r'(?!ADC_IN)(?P<wabe_id>[\w\d-]+)[_]'
                        r'(?P<wabe_type>[12345S])[.]'
                        r'(?P<sensor_name>[\w\d]+).*')


@app.route('/logger_ui/name_builder', methods=['POST', 'GET'])
def html_ui_namebuilder():
    global current_values, site_information

    if request.method == 'POST':
        if 'buildnames_btn' in request.form:
            # Follow naming convention
            loc = sanitize_name(request.form['location_name'])
            spring = sanitize_name(request.form['wabe_spring']) \
                if request.form['wabe_spring'] else request.form['wabe_id']
            device_name = '.'.join([loc,
                                    request.form['location_building'],
                                    spring])

            sensor_name = f"{request.form['wabe_id']}_{request.form['wabe_type']}" \
                          + f".{request.form['sensor_name']}"
            pressure_ch = sensor_name + '.pressure(V)'
            idx = int(request.form['pressure_ch'])
            pressure_ch = ([x for x, y in channel_idx.items() if y == idx][0],
                           pressure_ch)
            temp_ch = sensor_name + '.temperature(V)'
            idx = int(request.form['temperature_ch'])
            temp_ch = ([x for x, y in channel_idx.items() if y == idx][0],
                       temp_ch)

            # Set values on defaults
            current_values['device_name'] = device_name
            for chname in channel_idx.keys():
                current_values[chname] = ''
            current_values[pressure_ch[0]] = pressure_ch[1]
            current_values[temp_ch[0]] = temp_ch[1]
            return redirect(url_for('html_ui_get'))
    else:
        if current_values['device_name']:
            m = devname_re.match(current_values['device_name'])
            if m is not None:
                devname_dict = m.groupdict()
                site_information.update(devname_dict)

        got_sname = False
        for chname, idx in channel_idx.items():
            m = senname_re.match(current_values[chname])
            if m is not None and (not got_sname):
                senname_dict = m.groupdict()
                site_information.update(senname_dict)
                got_sname = True
            if 'pressure' in current_values[chname]:
                site_information['pressure_ch'] = str(idx)
            elif 'temp' in current_values[chname]:
                site_information['temperature_ch'] = str(idx)

        for field in ('location_name', 'wabe_spring'):
            site_information[field] = sanitize_name(site_information[field],
                                                    reverse=True)
        return render_template('logger_ui_namebuilder.html', **site_information)


def sample_to_record(sample):

    sample_rcd = sample._asdict()
    # format datetime into str
    dt_str = sample_rcd['datetime'].strftime(datetime_fmt)
    # add units and make str
    sample_rcd = {f'{k} {"["+u+"]" if (u := getattr(sui.sample_siunits, k)) else ""}': f'{v}'
                  for k, v in sample_rcd.items() if not k.startswith('date')}
    sample_rcd[f'datetime [{datetime_fmt}]'] = dt_str

    return sample_rcd


@app.route('/logger_ui/samples', methods=['GET'])
def html_ui_samples():
    if not sui.logger_present():
        return connection_lost()

    if 'connect_btn' in request.args:
        return goto_connect()
    if 'config_btn' in request.args:
        return goto_config(reload_btn='')

    if 'manual_sample_btn' in request.args:
        # FIXME: remove workaround for issue #34
        # In what repository is that issue?
        sui.manual_sample()

    total, new = sui.get_sample_count()
    if 'n_samples' in request.args:
        n_samples = int(request.args['n_samples'])
        n_samples = max(1, n_samples)  # discard 0 samples
    else:
        n_samples = max(min(25, total), 5)

    samples = []
    index = list(range(total - n_samples, total))
    for i in index:
        samples.append(sample_to_record(sui.get_sample(sample=i)))

    sample_df = pd.DataFrame().from_records(samples)
    sample_df.index = index
    if not sample_df.empty:
        # Only do this if there is data in the df, i.e. there are samples
        dt_col = sample_df.columns[sample_df.columns.str.startswith('date')][0]
        sample_df.sort_values(by=dt_col, ascending=False, inplace=True)
        col_oder = [dt_col] + sample_df.columns[sample_df.columns != dt_col].to_list()
        sample_df = sample_df[col_oder]
        #sample_df = sample_df.set_index(dt_col, drop=True)

    interval, units = sui.get_sampling_interval()
    interval = f"{interval} {'min' if units == 'T' else units}"

    df_html = '<p>There is no data in the loger.<br>Wait approx. sampling interval and reload the page.</p>'\
              if sample_df.empty else sample_df.to_html()
    return render_template('logger_ui_samples.html', sample=df_html,
                                                sample_count=(total, new),
                                                auto_sampleStatus=sui.get_auto_sampling(),
                                                sampling_interval=interval,
                                                n_samples=n_samples,
                                                )


if __name__ == "__main__":
    app.run()

"""
    Functional serial interface to WABEsense data-logger

    Available commands (serial command: module function name)

    Getters
        .. pyexec::
            from dataloggerui.commands import GetterDict as d
            d = {v.name: 'get_'+k for k, v in d.items()}
            for k, v in sorted(d.items()):
                if 'rtc_time' in v:
                    v = v.replace('rtc_time', 'datetime')
                print(f'{k}: :func:`~.{v}`\\n')

    Setters
        .. pyexec::
            from dataloggerui.commands import SetterDict as d
            d = {v.name: 'set_'+k for k, v in d.items()}
            for k, v in sorted(d.items()):
                if 'rtc_time' in v:
                    v = v.replace('rtc_time', 'datetime')
                print(f'{k}: :func:`~.{v}`\\n')

    Actions
        .. pyexec::
            from dataloggerui.commands import ActionDict as d
            d = {v.name: k for k, v in d.items()}
            for k, v in sorted(d.items()):
                print(f'{k}: :func:`~.{v}`\\n')
"""

# Copyright (C) 2021 OST Ostschweizer Fachhochschule
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import datetime
import re

from pkg_resources import packaging
from itertools import chain
from collections import namedtuple

try:
    from .commands import ActionDict, SetterDict, GetterDict
    from .logger import (datetime_fmt_rtc_set, datetime_fmt_rtc_get,
                         datetime_fmt_sample, channel_idx)
except ImportError:
    from commands import ActionDict, SetterDict, GetterDict
    from logger import (datetime_fmt_rtc_set, datetime_fmt_rtc_get,
                         datetime_fmt_sample, channel_idx)

import serial
import serial.tools.list_ports as slp

wbsLogger = serial.Serial(timeout=0)
""" Default logger object of class :class:`serial.Serial`. """


_control_chars = ''.join(map(chr, chain(range(0x00, 0x20),
                                        range(0x7f, 0xa0))))
_control_char_re = re.compile(f'[{re.escape(_control_chars)}]')

CommandAnswer = namedtuple('CommandAnswer', ['name', 'data_length', 'data'])
""" Data structure to hold answers from serial commands. """


def run_command(cmd, *, logger=None, raw_answer=False):
    """ Run command over serial port.

    Parameters
    ----------
    cmd: :class:`str`
        Command to run. See :class:`~dataloggerui.commands.Command`
    logger: :class:`serial.Serial`
        The serial port instance connected to the logger. The default is
        :obj:`.wbsLogger`
    raw_answer: :class:`bool`
        Whether to return the raw aswer from the logger.

    Returns
    -------
    :class:`bytes`
        If ``raw_answer`` is True
    :class:`.CommandAnswer`
        If ``raw_answer`` is False

    Raises
    ------
    ValueError
        If the logger port is not open
    """
    if logger is None:
        logger = wbsLogger

    if not logger.is_open:
        raise ValueError('The port is not open.')

    logger.write(cmd)
    logger.flush()

    answer = None
    while answer is None:
        while logger.in_waiting > 0:
            answer = logger.read(logger.in_waiting)
            logger.reset_input_buffer()
            if not raw_answer:
                answer = CommandAnswer(name=answer[1:4].decode(),
                                       data_length=answer[4],
                                       data=answer[5:-1].decode())
    return answer


SerialPortInfo = namedtuple('SerialPortInfo', ['port', 'description'])


def list_logger_ports():
    """ Lists ports that are likely to be a WABEsense logger.

    """
    ports = []
    for p in slp.comports():
        could_be = 'STM' in p.description
        if p.manufacturer is not None:
            could_be |= 'STM' in p.manufacturer
        if p.product is not None:
            could_be |= 'STM' in p.product
        if p.hwid is not None:
            pidString = "PID=0483:5740"
            if pidString in p.hwid:
                could_be |= pidString in p.hwid
        if could_be:
            ports.append(SerialPortInfo(port=p.device,
                                        description=p.description))

    return ports


def list_ports():
    """ List serial ports.

        A simple wrapper around :func:`~serial.tools.list_ports.comports`
    """

    return [SerialPortInfo(port=x.device, description=x.description)
            for x in slp.comports()]


def logger_present():
    myports = [p.device for p in serial.tools.list_ports.comports()]
    return wbsLogger.port in myports


def connect_logger(port=None):
    """ Connect the default logger to the given serial port name.
    
        The default logger is :obj:`.wbsLogger`.

        Parameters
        ----------
        port: class:`str`
            Name of the serial port to connect to. If not given an automatic
            connection is attempted. The first serial port returning a device
            name is accetped.
    """
    ports = list_logger_ports() if port is None \
                                else [SerialPortInfo(port=port, description='User defined port')]
    for p in ports:
        wbsLogger.port = p.port
        wbsLogger.open()
        
        # reaches here only iof connection was successfule
        print(f'Connected to {p.description}')
        break

def disconnect_logger(port=None):
    """ Disconnect the default logger.
    
        The default logger is :obj:`.wbsLogger`.
    """
    wbsLogger.close()


# -------------
# Getters
# -------------
def get_device_name():
    """ Get device name over the serial port.

    Returns
    -------
    :class:`str`
        Device name
    """
    cmd = GetterDict['device_name']()
    # get only the device name
    return run_command(cmd).data


def get_sampling_interval():
    """ Get sampling interval value over the serial port.

    Returns
    -------
    :class:`int`
        Sampling interval
    """
    cmd = GetterDict['sampling_interval']()
    answer = run_command(cmd)
    # get only the sample interval
    interval = int(answer.data)
    # get units
    fw_ver = get_firmware_version()
    if packaging.version.parse(fw_ver) < packaging.version.parse('1.0.0'):
        # Only minutes supported
        units = "T"
    else:
        raise NotImplemented('Sampling units not implemented yet.')

    return interval, units


def get_sample_count():
    """ Get number of samples stored in logger over the serial port.

    Returns
    -------
    total: :class:`int`
        Total number fo samples in the internal storage
    new: :class:`int` or :class:`None`
        Number of samples not yet downloaded or read-out

        .. note::
            Not None only for firmware version > 0.1.2
    """
    cmd = GetterDict['sample_count']()
    answer = run_command(cmd)
    # get only the sample counts
    counts = answer.data

    fw_ver = get_firmware_version()
    if packaging.version.parse(fw_ver) <= packaging.version.parse('0.1.2'):
        return int(counts), None
    else:
        total, new = map(int, counts.split(','))
        return total, new


def get_auto_sampling():
    """ Get state of auto sampling (logging) over the serial port.

    Returns
    -------
    :class:`bool`
        Whether auto sampling (logging) is active
    """
    cmd = GetterDict['auto_sampling']()
    answer = run_command(cmd).data
    if answer == '0':
        return False
    else:
        return True


def get_battery_configuration():
    """ Get battery configuration over the serial port.

    Returns
    -------
    :class:`float`
        Battery cell voltage in volts
    :class:`int`
        Number of battery cells

    """
    cmd = GetterDict['battery_configuration']()
    # get only the voltage
    bat_conf = run_command(cmd).data
    mv, count = bat_conf.split(',')
    volt = float(mv) / 1000.0
    count = int(count)
    return volt, count


def get_firmware_version():
    """ Get firmware version over the serial port.

    Returns
    -------
    :class:`str`
        Firmware version
    """
    cmd = GetterDict['firmware_version']()
    # get only the version
    return run_command(cmd).data


def get_unique_id():
    """ Get unique ID over the serial port.

    Returns
    -------
    :class:`str`
        Unique ID
    """
    
    fw_ver = get_firmware_version()
    if packaging.version.parse(fw_ver) < packaging.version.parse('0.1.4'):
        return 'Not implemented'
    else:
        cmd = GetterDict['unique_id']()
        answer = run_command(cmd).data
        
        # get only the ID
        return answer[:26]


def get_analog_channel_name(*, channel=None):
    """ Get analog channel name(s) over the serial port.

    Parameters
    ----------
    channel: :class:`int` or :class:`str` or None
        Channel number to read. Valid values: 0-3
        If None (default) then all channel names are retrieved.

    Returns
    -------
    :class:`str`
        Channel name.
        If name is ``NA`` it means that an empty named was set.

    Raises
    ------
    ValueError:
        If channel is not valid

    """
    valid_channels = '0123'
    if channel is None:
        ch_name = []
        for ch in valid_channels:
            cmd = GetterDict['analog_channel_name'](channel=ch)
            # append only the name
            ch_name.append(run_command(cmd).data)
    else:
        ch_str = f'{channel}'
        if ch_str not in valid_channels:
            raise ValueError(f'Invalid channel number {channel}')
        cmd = GetterDict['analog_channel_name'](channel=ch_str)
        # get only the name
        ch_name = run_command(cmd).data

    return ch_name


def parse_datetime(dt_str, *, fmt):
    """ Parse datetime string into datetime.
    
    Parameters
    ----------
    dt_str: :class:`str`
        Datetime as string
    fmt: :class:`str`
        Format string to use for parsing.
    
    Returns
    -------
    :class:`datetime.datetime`
    """
    return datetime.datetime.strptime(dt_str, fmt).replace(tzinfo=datetime.timezone.utc)


LoggerSample = namedtuple('LoggerSample', ['datetime',
                                           'autosample',
                                           'battery_voltage',
                                           'bme_temperature',
                                           'bme_pressure',
                                           'bme_humidity',
                                           'adc_in8',
                                           'adc_in9'])
""" Inmutable logger sample container. """


sample_typecast = LoggerSample(*([lambda u: parse_datetime(u, fmt=datetime_fmt_sample),
                                  lambda u: u == '1'] + [float]*6))
""" Functions to cast strings into sample data types """

sample_siunits = LoggerSample(*[datetime_fmt_sample, '', 'V', 'C', 'Pa', '%', 'V', 'V'])
""" SI units of a logger sample """


def get_sample(*, sample=None, as_str=False):
    """ Get sample value over the serial port.

    Parameters
    ----------
    sample: :class:`int` or :class:`str`
        Zero indexed position of the sample, e.g. first sample is index = 0
        last sample is index = total samples - 1
    as_str: :class:`bool`
        Whether the sample should be returned as a string instead of parsed 
        into a :class:`.LoggerSample`

    Returns
    -------
    sample: :class:`.LoggerSample` or :class:`str`
        Sample values. It is a string of ``as_str`` is True.

    Raises
    ------
    ValueError:
        If index is equal or greater than total samples
    RuntimeError:
        If command fails: logger returns ``NAK``

    See also
    --------
    :func:`~.get_sample_count`
    
    """
    idx_str = f'{sample:06}' if sample is not None else ''
    cmd = GetterDict['sample'](sample=idx_str)
    sample_str = run_command(cmd).data
    if 'nak' in sample_str.lower():
        total, _ = get_sample_count()
        if sample > (total - 1):
            raise ValueError(f'Sample index too large {sample} > {total - 1}')
        else:
            raise RuntimeError('Logger returned NAK')
    if as_str:
        return sample_str
    else:
        sample_str = sample_str.split(';')
        # TODO: channel names
        return LoggerSample(*(tt(v) for v, tt in zip(sample_str, sample_typecast)))


def get_datetime():
    """ Get date and time over the serial port.

    Returns
    -------
    :class:`datetime.datetime`
        Date and time
    """
    cmd = GetterDict['rtc_time']()
    # get only the data
    return parse_datetime(run_command(cmd).data,
                          fmt=datetime_fmt_rtc_get)


def get_configuration():
    """ Get current logger configuration.

        The current configuration might not be stored to the non-volatile
        memory of the logger.

        Returns
        -------
        :class:`dict`
            Current logger configurationDate and time

        See also
        --------
        :func:`~.store_configuration()`
            Make current configuration permanent (non-volatile).

    """
    volt, count = get_battery_configuration()
    interval, units = get_sampling_interval()
    config_dict = dict(
        device_name=get_device_name(),
        sampling_interval=interval,
        interval_units=units,
        auto_sampleStatus=get_auto_sampling(),
        samples_count=get_sample_count(),
        battery_cell_voltage=volt,
        battery_cell_count=count,
        firmware_version=get_firmware_version(),
        unique_id=get_unique_id(),
        rtc_now=get_datetime(),  # print time in LOCALE
        serial_port=wbsLogger.port,
    )
    # channel names
    ch_name = get_analog_channel_name()
    for ch, idx in channel_idx.items():
        config_dict[ch] = ch_name[idx]

    return config_dict


# -------------
# Setters
# -------------
def set_device_name(name):
    """ Set device name over the serial port

    Parameters
    ----------
    name: :class:`str`
        Device name, maximum length 92 characters

    Raises
    ------
    RuntimeError
        If command fails: logger returns ``NAK``
    ValueError
        If name is longer than 92 characters
    """
    if len(name) > 92:
        raise ValueError(f'Device name too long {len(name)} > 92')

    cmd = SetterDict['device_name'](name=name)
    answer = run_command(cmd).data
    if 'nak' in answer.lower():
        raise RuntimeError(f"Failed to set device name {name}")

def set_data_stream(stream=False):
    """ Enable/Disable data streaming over serial interface.
    If enabled, one measurement after another is executed and values are streamed over serial interface.
    Data-Output is the same as MVG-Command is executed.    

    Parameters
    ----------
    stream: :class:`bool`, optional (default False)
        False to disable the data streaming. True to enable it.
        If not given, it defaults to False, i.e. the data streaming is disabled.

    Raises
    ------
    RuntimeError
        If command fails: logger returns ``NAK``
    """
    
    cmd = SetterDict['data_stream'](streaming='1' if stream else '0')
    answer = run_command(cmd).data
    if 'nak' in answer.lower():
        raise RuntimeError("Failed to set data stream enable/disable")


def enable_data_stream():
    """ Alias for :func:`~.set_data_stream` with True argument"""
    set_data_stream(True)


def disable_data_stream():
    """ Alias for :func:`~.set_data_stream` with False argument"""
    set_data_stream(False)


def set_sampling_interval(interval):
    """ Set sampling interval over the serial port

    Parameters
    ----------
    interval: :class:`int`
        Sampling interval in minutes

    Raises
    ------
    RuntimeError
        If command fails: logger returns ``NAK``
    """
    str_interval = f'{interval:04}'
    cmd = SetterDict['sampling_interval'](interval=str_interval)
    answer = run_command(cmd).data
    if 'nak' in answer.lower():
        raise RuntimeError(f"Failed to set sampling interval {str_interval}")


def set_battery_configuration(*, cell_voltage, cell_count):
    """ Set battery configuration over the serial port

    Parameters
    ----------
    cell_voltage: :class:`float`
        Cell voltage in volts
    cell_count: :class:`int`
        Number of battery cells

    Raises
    ------
    RuntimeError
        If command fails: logger returns ``NAK``
    ValueError
        If cell_voltage > 9.999 volts or negative, or cell_count > 9 or cell_count < 0
    """
    if (cell_voltage > 9.999) or (cell_voltage < 0):
        raise ValueError(f'Wrong cell voltage {cell_voltage}')
    if (cell_count > 9) or (cell_count < 0):
        raise ValueError(f'Wrong number of cells {cell_count}')

    mV = int(cell_voltage * 1000.0)
    cmd = SetterDict['battery_configuration'](cell_voltage=mV,
                                              cell_count=cell_count)
    answer = run_command(cmd).data
    if 'nak' in answer.lower():
        raise RuntimeError(f"Failed to battery configuration V:{cell_voltage} "
                           f"CC:{cell_count}")


def set_analog_channel_name(*, channel, name):
    """ Set name of analog channel over the serial port

    Parameters
    ----------
    channel: :class:`int`
        Channel number from 0 to 3
    name: :class:`str`
        Channel name, maximum length 64 characters.
        If it is empty, then ``NA`` is written to the logger.

    Raises
    ------
    RuntimeError
        If command fails: logger returns ``NAK``
    ValueError
        If channel out of range or name too long.
    """
    if (channel > 3) or (channel < 0):
        raise ValueError(f'Wrong channel number {channel}')

    if not name:
        name = 'NA'
    elif len(name) > 64:
        raise ValueError(f'Channel {channel} name too long {len(name)} > 64')
    cmd = SetterDict['analog_channel_name'](channel=channel, name=name)
    answer = run_command(cmd).data
    if 'nak' in answer.lower():
        raise RuntimeError(f"Failed to set channel {channel} name {name}")


def set_auto_sampling(auto=True):
    """ Set RTC time base sampling mode

    Parameters
    ----------
    auto: :class:`bool`, optional (default True)
        True to enable RTC time based interval sampling, False to disable it.
        If not given it defaults to True, i.e. the automatic sampling is activated.

    Raises
    ------
    RuntimeError
        If command fails: logger returns ``NAK``
    """

    cmd = SetterDict['auto_sampling'](active='1' if auto else '0')
    answer = run_command(cmd).data
        
    if 'nak' in answer.lower():
        raise RuntimeError("Failed to set auto flag (RTC interval based measurements)")


def enable_auto_sampling():
    """ Alias for :func:`~.set_auto_sampling` with True argument"""
    set_auto_sampling(True)


def disable_auto_sampling():
    """ Alias for :func:`~.set_auto_sampling` with False argument"""
    set_auto_sampling(False)


def set_datetime(dt):
    """ Set RTC clock date and time over serial port

    Parameters
    ----------
    dt: :class:`~datetime.datetime`
        Date and time

    Raises
    ------
    RuntimeError
        If command fails: logger returns ``NAK``
    """
    str_dt = dt.strftime(datetime_fmt_rtc_set)
    print("set: ", str_dt)
    cmd = SetterDict['rtc_time'](datetime=str_dt)
    answer = run_command(cmd).data
    if 'nak' in answer.lower():
        raise RuntimeError(f"Failed to set time '{str_dt}'")


# -------------
# Actions
# -------------
def store_configuration():
    """ Store configuration in non-volatile storage of the logger.

    Raises
    ------
    RuntimeError
        If command fails: logger returns ``NAK``
    """
    if 'nak' in run_command(ActionDict['store_configuration']()).data.lower():
        raise RuntimeError('Failed to store configuration')


def sync_rtc():
    """ Synchronize Logger RTC with PC clock in UTC

    The synchronization is persistent. That is, this function calls
    :func:`~.store_configuration` before returning.

    Returns
    -------
    :class:`datetime.datetime`
        Datetime of the logger's RTC clock after synchronization
        
    Raises
    ------
    RuntimeError
        If command fails: logger returns ``NAK``
    
    See also
    --------
    :func:`~.store_configuration`
    :func:`~.get_datetime`
    """
    str_dt = datetime.datetime.now(datetime.timezone.utc).strftime(datetime_fmt_rtc_set)
    print("sync: ", str_dt)
    # pass the logger to execute write as fast as possible
    answer = run_command(SetterDict['rtc_time'](datetime=str_dt),
                         logger=wbsLogger).data
    if 'nak' in answer.lower():
        raise RuntimeError(f"Failed to set time '{str_dt}'")
    # make sync permanent
    store_configuration()

    return get_datetime()


def manual_sample(**kwargs):
    """ Take a manual sample over the serial port 
    
    Returns
    -------
    :class:`.LoggerSample`

    Raises
    ------
    RuntimeError
        If command fails: logger returns ``NAK``
    
    See also
    --------
    :func:`~.get_sample`
    """
    answer = run_command(ActionDict['manual_sample'](), logger=wbsLogger).data
    if 'nak' in answer.lower():
        raise RuntimeError(f"Failed to take manual sample")
    return get_sample(**kwargs)


def data_clear():
    """ Delete all samples from the logger's internal storage.
    
    Raises
    ------
    RuntimeError
        If command fails: logger returns ``NAK``
    """
    answer = run_command(ActionDict['data_clear']()).data
    if 'nak' in answer.lower():
        raise RuntimeError(f"Failed to delete samples")


delete_samples = data_clear
""" Alias for :func:`~.data_clear`. """


def data_cursor_rewind():
    """ Rewind the data readout cursor, so on next data export all stored data are 
        exported again, not only the not already exported.
    
    Raises
    ------
    RuntimeError
        If command fails: logger returns ``NAK``
    """
    answer = run_command(ActionDict['data_cursor_rewind']()).data
    if 'nak' in answer.lower():
        raise RuntimeError(f"Failed to rewind data cursors")


def start_factory_bootloader():
    """ Exit the user application and jumps to the factory integrated USB-Bootloader (DFU USB Device).
    
    Raises
    ------
    RuntimeError
        If command fails: logger returns ``NAK``
    """
    answer = run_command(ActionDict['start_factory_bootloader'](key=bytes.fromhex('78345612').decode('utf-8'))).data
    if 'nak' in answer.lower():
        raise RuntimeError(f"Failed to start factory USB DFU bootloader")


def get_data_to_pandas(*, from_sample=0, resample=True):
    """ Get series of smaples and return a :class:`pandas.DataFrame`.
    
    Each sample is retieved using :func:`~.get_sample` on the default logger.
    Retrieving many samples can take a long time.

    Parameters
    -----------
    from_sample: int
        Index of the starting sample. It should be lower than :func:`~.get_sample_count`:code:`[1]`
    
    resample: bool
        Whether the data frame should eb resample to the sampling interval of the
        logger, as returned by :func:`~.get_sampling_interval`.

    Returns
    -------
    :class:`pandas.DataFrame`

    See also
    --------
    :func:`~.get_sample`
    :func:`~.get_sampling_interval`
    """

    n_total = s.get_sample_count()[1]

    df = []
    for n in range(from_sample, n_total):
        df.append(s.get_sample(sample=n)._asdict())
    df = pd.DataFrame.from_records(df).set_index('datetime')

    if resample:
        sampling = s.get_sampling_interval()
        s_str = f'{sampling[0]:d}{sampling[1]}'
        df = df.rolling(s_str).mean().resample(s_str).mean()

    return df

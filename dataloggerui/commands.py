"""
    List of commands and related functionality

    Available commands (serial command: dictionary key)

    Getters
        .. pyexec::
            from dataloggerui.commands import GetterDict as d
            d = {v.name: k for k, v in d.items()}
            for k, v in sorted(d.items()):
                print(f'{k}: {v}\\n')

    Setters
        .. pyexec::
            from dataloggerui.commands import SetterDict as d
            d = {v.name: k for k, v in d.items()}
            for k, v in sorted(d.items()):
                print(f'{k}: {v}\\n')

    Actions
        .. pyexec::
            from dataloggerui.commands import ActionDict as d
            d = {v.name: k for k, v in d.items()}
            for k, v in sorted(d.items()):
                print(f'{k}: {v}\\n')

"""

# Copyright (C) 2021 OST Ostschweizer Fachhochschule
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Adrian Tüscher <adrian.tuescher@ost.ch>
import sys

from string import Template
from collections.abc import Sequence
from typing import Union

class Command:
    """ Class for representing serial command accepted by the data-logger.

    """
    _endbyte = '\x03'
    _inibyte = '\x02'
    _encoding = 'ascii'

    def __init__(self, name: str, args: Union[None, str, Sequence] = None,
                 args_len_bounds: Union[int, Sequence] = 0,
                 doc: str = '',):
        self.name = name
        self.__doc__ = doc

        if args is not None:
            self.args = (args, ) if isinstance(args, str) else args
        else:
            self.args = tuple()

        self.args_len_bounds = (args_len_bounds, )*2 \
            if isinstance(args_len_bounds, int) else args_len_bounds

        tpl_ = '$argslen$args' if self.args else '\x00'
        # The templated structure of a command
        cmd = self._inibyte + self.name + tpl_ + self._endbyte

        self._cmd_template = Template(cmd)
        self._args_template = Template(','.join('$' + x for x in self.args))

    def __str__(self) -> str:
        kwargs = {k: k for k in self.args}
        return self._cmd_template.substitute(args=self._args_template.template,
                                             argslen='(args_len)')

    def __repr__(self):
        return f'Command<{str(self).encode(self._encoding)}>'

    def __call__(self, **kwargs):
        args = self._args_template.substitute(**kwargs)
        largs = len(args.encode(self._encoding))

        if largs < self.args_len_bounds[0]:
            msg = f'{self.name}: argument {args} is too short ' \
                  f'({largs}<{self.args_len_bounds[0]})'
            raise ValueError(msg)

        if largs > self.args_len_bounds[1]:
            msg = f'{self.name}: argument {args} is too long ' \
                  f'({largs}>{self.args_len_bounds[1]})'
            raise ValueError(msg)

        argslen = hexlen_str(args)
        cmd = self._cmd_template.substitute(args=args, argslen=argslen)
        return cmd.encode(self._encoding)


def hexlen_str(string: str, encoding: str ='ascii') -> str:
    """ Number of bytes in the encoded string in hex base.

        Parameters
        ----------
        string: :class:`str`
        encoding: :class:`str`

        Returns
        -------
        :class:`str`
    """
    hlen = hex(len(string.encode(encoding)))[2:]
    hlen = '0' + hlen if len(hlen) == 1 else hlen
    return bytes.fromhex(hlen).decode(encoding)


GetterDict = dict(
    device_name=Command('DNG',
                        doc='''
The name of the logger, the maximum length of the string is 92 characters.
'''),
    sampling_interval=Command('MIG',
                              doc='''
Current sampling interval in minutes as a 4 characters string from 
``0001`` to ``1092``. 
The string ``0000`` means that auto-sampling is disabled.
'''),
    sample_count=Command('MNG',
                         doc='''
Number of samples stored in the logger's internal storage as a 6 characters
string from ``000000`` to ``131072``
nrOfAbsoluteStoredSamples:  The absolute number of stored samples on the internal storage.
nrOfNewStoredSamples:       The number of new samples, which are not readout (exported to SD-Card).

<nrOfAbsoluteStoredSamples>,<nrOfNewStoredSamples>

example: '000005,000000'   : a total of 5 samples are stored, no new samples are stored since the last data readout.
'''),
    sample=Command('MVG', 'sample', (0, 6),
                         doc='''
If no argument is given the last record is returned, otherwise a 6 
characters string is needed, representing the requested measurement data
point number.
See :ref:`sample_count <sample-count>`.

RTC_Date(yyyy.mm.dd hh:mm:ss.fff);
  <*Measurement_Auto*>;Battery_Voltage(V);
  BME_Temperature(C);BME_Pressure(Pa);BME_Humidity(%);ADC_IN8(V);ADC_IN9(V);ADC_IN10(V);ADC_IN11(V)

<*Measurement_Auto*>: {'0': manual measurement, '1': RTC time based measurement}           

example: '2021.03.22 09:16:22.199;0;7.132812;23.437500;96792.335937;36.806640;3.480591;0.000000;0.000000;0.000000'
'''),
    rtc_time=Command('RTG',
                     doc='''
RTC_Date(yyyy.mm.dd) *<weekday>* RTC_Time(hh:mm:ss.ms)

weekday {'1': monday, '2': tuesday, '3': wednesday, '4': thursday, '5': friday, '6':saturday, '7':sunday}

example: '2021.02.26 5 09:25:52.378'
'''),
    battery_configuration=Command('BCG',
                                  doc='''
Nominal cell voltage(mV)(4 characters)>, <number of cells(1 character)

note: For external DC-PowerSource, the number of battery cells needs to be set to '0'. The nominal cell voltage is don't care in this case.

example: '3700,2' - two battery cells, each with 3700mV (3.7V)

example: '3700,0' - no battery cells --> external DC-PowerSource (see description above)

'''),
    analog_channel_name=Command('ANG', 'channel', 1,
                                doc='''
It needs a single character argument {'0','1','2','3'} representing the 
analog channel number.
Returns the channel's name as a string of maximum length of 64 characters
'''),
    firmware_version=Command('FVG',
                             doc='''
major.minor.bug

example: '0.1.2'

'''),
    auto_sampling=Command('ASG',
                             doc='''
Returns the autoSampling flag {'0': no automatic RTC time based measurements, '1': RTC time based measurement is active}.

'''),
    unique_id=Command('UIG',
                             doc='''
Returns the unique system identification number as hex formated string (length of answer-data is fix with 26 characters).

example: '0x00320032425350112035354E'

''')
)
""" Collection of getter commands.

    All getter commands return the string ``NAK`` if the serial command is not 
    valid or the input parameters are missing or wrong.
 
    Below is the list of available commands:
    
    .. pyexec::
            from textwrap import indent
            from dataloggerui.commands import GetterDict as d
            for k, v in sorted(d.items()):
                print(f'.. _{k.replace("_","-")}:')
                print()
                print(f'**{k}**:')
                print(indent(v.__doc__, ' '*4))
                print()
"""

SetterDict = dict(
    device_name=Command('DNS', 'name', (1, 92),
                        doc='''
name: :class:`str`: Name for the device. maximum length 92 characters.

.. note::
   Only variables in RAM are update, on a power-loss settings reset to default, 
   for persistent configuration, see :ref:`store_configuration <store-configuration>` 
   in :obj:`~.ActionDict`
'''
),
    sampling_interval=Command('MIS', 'interval', 4,
                              doc='''
The sampling interval in minutes

4 characters {'0001' to '1092'}

.. note::
    Only variables in RAM are update, on a power-loss settings reset to default, for persistent configuration, see store_configuration in ActionDict
'''),
    rtc_time=Command('RTS', 'datetime', 21,
                     doc='''
year(4digits).month(2characters) *'space'* *<weekday>* (1character) *'space'* hours(2characters):minutes(2characters):seconds(2characters)

weekday {'1': monday, '2': tuesday, '3': wednesday, '4': thursday, '5': friday, '6':saturday, '7':sunday}

.. note::
    The time of the RTC unit is directly updated over this setter command. A CR2032 backup battery needs to be installed before to backup the RTC power-domain.

example: '2021.02.02 2 07:49:00'
'''),
    battery_configuration=Command('BCS', ('cell_voltage', 'cell_count'), 6,
                                  doc='''
nominal cell voltage(mV)(4 characters)>, <number of cells(1 character)

.. note::
    Only variables in RAM are update, on a power-loss settings reset to default, for persistent configuration, see store_configuration in ActionDict

example: '3700,2'
'''),
    analog_channel_name=Command('ANS', ('channel', 'name'), (2, 64),
                                doc='''
analog representing channel name (1 to 64 characters)
Name cannot be empty

.. note::
    Only variables in RAM are update, on a power-loss settings reset to default, for persistent configuration, see store_configuration in ActionDict
'''),
    data_stream=Command('DSS', 'active', 1,
                        doc='''
activate/deactivate fast data streaming over serial port {'0': disable, '1': enable}

.. note::
    The streamed data are not stored in the FLASH of the datalogger. They are just sent out once over the virtual COM-Port.
'''),
    auto_sampling=Command('ASS', 'active', 1,
                        doc='''
activate/deactivate RTC-Time based automatic sampling {'0': disable, '1': enable}.

.. note::
    This flag is stored in a RTC-Backup register. A CR2032 backup battery needs to be installed before to backup the RTC power-domain.
''')
)
""" Collection of setter commands.

    All setter commands return the string ``NAK`` if the serial command is not 
    valid or the input parameters are missing or wrong. 
    If serial command is valid and was accepted the return string is ``ACK``.

    Below is the list of available commands:

    .. pyexec::
            from textwrap import indent
            from dataloggerui.commands import SetterDict as d
            for k, v in sorted(d.items()):
                print(f'.. _set-{k.replace("_","-")}:')
                print()
                print(f'**{k}**:')
                print(indent(v.__doc__, ' '*4))
                print()
"""

ActionDict = dict(
    store_configuration=Command('CSA',
                                doc='''
Store all currently setup configuration in the logger's internal storage (persistent).

Note: All the following configurations are only set in RAM over the corresponding setter command. For persistent configuration, they need
to be stored with this command.
* analog_channel_name
* battery_configuration
* device_name
* sampling_interval
'''),
    data_clear=Command('DCA',
                       doc='''
Clear all measurement data located in the the logger's internal storage (measurement number getting zero).

Clear all history data located in the the logger's internal storage.
'''),
    data_cursor_rewind=Command('CRA',
                               doc='''
Rewind the data cursor, so on next data export all stored data are 
exported again, not only the not already exported.
'''),
    manual_sample=Command('MMA',
                   doc='''
Trigger a manual measurement over the serial interface. The measurement is logged
with the flag indicating automatic measurement set to 0.
'''),
    # This will be removed from the serial UI
    start_factory_bootloader=Command('UBA', 'key', 4,
                   doc='''
.. note::            
    This command does not return ``ACK`` on success

Exit the user application and jumps to the factory integrated USB-Bootloader (DFU USB Device).

Just a small key is needed to successful enter the bootloader (4 
characters) {hexadecimal: ``0x78345612``}.

A firmware update can be executed with the STM32CubeProgrammer 
application (details see in user manual)
After a successful update, power needs to be removed from the data-logger 
(without the CR2032 backup battery)
'''),
)
""" Collection of action commands.

    All action commands return the string ``NAK`` if the serial command is not 
    valid or the input parameters are missing or wrong. 
    If serial command is valid and was accepted the return string is ``ACK``.

    Below is the list of available commands:

    .. pyexec::
            from textwrap import indent
            from dataloggerui.commands import ActionDict as d
            for k, v in sorted(d.items()):
                print(f'.. _{k.replace("_","-")}:')
                print()
                print(f'**{k}**:')
                print(indent(v.__doc__, ' '*4))
                print()
"""

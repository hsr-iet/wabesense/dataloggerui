=====================
Serial user interface
=====================

To communicate with the logger using the serial port use the following settings:

.. list-table:: Serial port settings
	:widths: 10 10
	:header-rows: 1

	*	- Setting
		- Value
	*	- Baudrate
		- 115200
	*	- Nr of data bits
		- 8
	*	- Parity
		- None
	*	- Stop bits
		- 1
	*	- Flow control
		- None


Sending commands directly using the serial port
===============================================

To send commands to the logger you need to write them to the serial port.
The commands have a fixed structure::

     <start character><3 letters command name><length of arguments><arguments><end character>

start, end character
    These are non-printable ascii characters.
    They hex values are ``\x02`` and ``\x03`` for the start and end character, respectively.

3 letters command name
    The name of the command. See `` for details.

length of arguments
    A hexadecimal number representing the number of characters used by the
    the input arguments.
    The maximum value is 92.
    The function :func:'~dataloggerui.commands.hexlen_str' can be used to get
    this value.

arguments
    The actual arguments. See `` for details.

Example
-------

Lets set the name of the device to ``Hello``. To do this, we use the ``DNS``
(Device-Name-Set) serial command (python function :ref:`set_device_name <set-device-name>`).
Write the following string literal to the serial port::

		\0x02DNS\x05Hello\0x03

Note that the payload (the length of the arguments) is 5 characters, and this is
passed right after the command name as an hex number.
If everything went well the contents of the serial port should be::

	\0x02ACK\0x00\0x03

indicating that the command was correctly processed.
Otherwise it should be::

	\0x02NAK\0x00\0x03

To get the name given to the connected logger, use the ``DNG`` (Device-Name-Get)
serial command (python function :ref:`device_name <device-name>`).
To execute this command write the following string literal to the serial port::

    \0x02DNG\0x00\0x03

The logger will write back to the serial port as string with the name of the device::

	\0x02DNG\0x05Hello\0x03


Sending commands using the python serial interface
==================================================

TODO

=====================
HTML user interface
=====================
.. contents:: :local:


This module provides an HTML user interface (henceforth just UI) to interact
with the logger.
The interface is experimental, so use with care and report bugs in the `issue tracker`_.

Currently the UI uses Flask_ to execute python code via HTML requests.
The page is served with a development server.

To start the server run::

    python -m dataloggerui.htmlui

then go to http://localhost:5000/logger_ui with your internet browser.

.. attention::
    Do not close the terminal where the server is running.
    The server provides (*serves*) the HTML user interface. If the server is not
    running, the HTMl user interface will not run.

Connection page
---------------

Once in http://localhost:5000/logger_ui, the page you will see looks like the figure below

.. _connection_page:

.. figure:: _static/htmlui_connect.png
    :alt: Serial port selection and connection

    Select the serial port to which the logger is connected or
    continue without a logger.

In this page you need to select the port to which the logger is connected.

The drop-down menu lists all serial ports that were detected.
The system tries to filter those that are likely to be used by WABEsense loggers.
Next to the port (e.g. ``/dev/ttyACM0``) you will see a description (if one was
found).
WABEsense loggers currently use a chip from **STM**\ icroelectronics, hence ports
with descriptions ``STM32 Virtual ComPort`` or similar, are likely to be used by
the logger.

Once you have selected a port click the **Connect** button to connect to the
logger.

You can also choose to work without a logger, for an explanation see `Working without a logger`_

Configuration page
------------------
If the connection is **not** successful you will be sent back into the `Connection
page`_ and an error will appear at the top of the page.

If the connection is successful you will see a page that looks like the figure
below,

.. figure:: _static/htmlui_overview.png
    :alt: Overview of the configuration page

    Overview of the configuration page.

This is the configuration page of the UI.
The page is segmented in different sections:

    - `Top buttons`_
    - `Configuration`_
    - `Device`_
    - `Analog channel names`_
    - `Logging`_
    - `Battery`_
    - `Internal clock`_

which you can recognize from the graphical representation of the UI.
The configuration page is composed of more than one `HTML form`_ that need to be filled to configure
the logger.

.. attention::
    Most fields and buttons provide some brief explanation of their meaning or function
    when you hover over them with the mouse pointer and wait a second.

Button types
^^^^^^^^^^^^^
There are three types of buttons in the UI:

**Form buttons**
    Form buttons look like this |form_btn|.
    These buttons change the information shown in the form or are used for navigation.
    They do not affect the logger in anyway.

**Data buttons**
    There are two types of data buttons.
    Those that modify the data on the logger look like this |upload_btn|.

    Those that modify the data on the computer disk look like this |data_btn|.

    The difference between the two buttons is color gradient in their backgrounds.
    When you hover over the button with the mouse pointer, the gradient moves.
    A motion to the **right** indicates that data will be **uploaded to** the logger.
    A motion to the **left** indicates that data will be **retrieved from** the logger
    or **stored in** the computer's disk.

The |set_btn| buttons in the different sections can be used to temporarily set values
in the configuration.
These changes will not persist after the logger is disconnected from its power
source, e.g. external batteries or USB cable.

.. warning::
    The configuration of the logger is only modified after the :ref:`Upload <upload-btn>`
    is pressed.
    The changes shown after |set_btn| buttons are pressed will not persist after
    the logger is disconnected from its power source.
    The only exception is the set button in the `Internal clock`_ section, which
    sets all the configurations to the persistent storage of the logger.

.. note::
    Pressing the **Enter** or **Return** key at any point is equivalent to pressing the :ref:`Upload <upload-btn>`
    button.

.. |form_btn| image:: _static/form_button.png
.. |data_btn| image:: _static/data_button.png
.. |upload_btn| image:: _static/upload_button.png
.. |set_btn| image:: _static/set_button.png

Top buttons
^^^^^^^^^^^

.. figure:: _static/top_buttons.png
   :alt: Top buttons


The buttons on the top of the UI perform the following actions

.. _get-config-btn:

**Get configuration from logger**
    Reads the configuration stored in the logger and fills the form in the
    UI using this data.

.. _clear-btn:

**Clear**
    Clear all the contents in the form (it does not affect the logger).

**Defaults**
    Fill the form using default values.
    Not all fields have a default value.
    User provided values of fields that do not have a default value are conserved.

**Connect logger**
    Return to the `Connection page`_.

**Name builder**
    Navigate to the `Name builder page`_, where you can set names that follow the `Naming convention`_
    of the WABEsense project.

**Load file**
    This button loads the contents of a selected file.
    To select the file press the **Choose file** button.
    Then press the **Load file** button to read the contents of the file into the
    form.

Configuration
^^^^^^^^^^^^^

.. figure:: _static/configuration_section.png
   :alt: Configuration section


This section is dedicated to the administration of the configuration of the logger.

.. _setall-btn:

**Set all**
    Set values of all sections into the logger's working memory (not persistent).
    This is the same as pressing all the **Set** buttons, except the one in
    the internal clock section.

    .. note::
        The internal clock is not set by this action. Use the buttons
        in the internal clock section to do it.

.. _upload-btn:

**Upload**
    When this button is pressed the current data from the UI is uploaded to the
    logger.
    After this action the logger is modified.
    You can check that everything went well by clearing the form using the
    :ref:`Clear <clear-btn>` button, and then getting the configuration from the
    logger using the :ref:`Get configuration <get-config-btn>` button.

    .. note::
        The internal clock is not uploaded by this action. Use the buttons
        in the internal clock section to do it.

.. role:: raw-html(raw)
   :format: html

**Save to file**
    This button saves the current data shown in the UI to a YAML file.
    After this action, the path to the file appears at the bottom of this section.
    The file is saved to the current working directory by default.

    At the bottom of these section a link toa file is shown. This link shows the location
    where the configuration save will be saved when the **Save to file** is pressed.
    If this :raw-html:`<font color="red">link is red</font>`, then the configuration has not been saved yet.
    When the configuration is saved, the :raw-html:`<font color="blue">link is blue</font>`.

Device
^^^^^^^^^^^

.. figure:: _static/device_section.png
   :alt: Device section


This section shows information about the device.
The only field that can be modified is the ``Name`` of the device.
The other fields show read-only information about the logger.

**Name**
    The name of the device.
    In the context of the WABEsense project all loggers should be named
    following the `Naming convention`_.
    The naming convention is shown in greyed characters when the device name is
    empty.

**Firmware version**
    The version of the firmware running on the data-logger.
    The firmware versioning follows the convention: major.minor.bug

**Unique ID**
    This is the unique ID of the logger. This number unequivocally identifies the
    logger.

Analog channel names
^^^^^^^^^^^^^^^^^^^^

.. figure:: _static/channels_section.png
   :alt: Analog channel section


This section is for the administration of the analog channel names.
The analog channels carry the data measured by the sensors connected to the logger.
In the context of the WABEsense project all channels should be named following
the `Naming convention`_.
The naming convention is shown in greyed characters when the channel name is empty.

Logging
^^^^^^^^^^^

.. figure:: _static/logging_section.png
   :alt: Logging section

This section is to configure the logging behavior of the logger.

**Interval**
    The sampling interval. An integer number indicating the time interval
    between two consecutive measurement.

**Units**
    This drop-down menu can be used to select the units of the interval.
    The default value is 10 minutes.

    .. note::
        Only minutes are support on Firmware version 1.0.0 or below


**Status**
    Whether logging is enabled or not.

    .. note::
        If logging is disabled, the logger will not take samples automatically.

**Records count**
    The number of samples stored in the internal storage of the logger.
    There are two numbers shown, the first one is the records that have not yet
    been downloaded, the second one is the total number of records in the
    logger's storage.

Battery
^^^^^^^^^^^

.. figure:: _static/battery_section.png
   :alt: Battery section

This section is used to indicate the battery configuration.

**Nominal voltage**
    The nominal voltage of a single cell (e.g. single battery) in volts.
    This number can have decimals, use a ``.`` (point) for the decimal places.

**Number of cells**
    The number of cells connected to the logger.

Internal clock
^^^^^^^^^^^^^^

.. figure:: _static/clock_section.png
   :alt: Internal clock section

This section shows the state of the internal clock.
The date and time corresponds to the moment when the button :ref:`Get configuration <get-config-btn>` or
**Get from logger** were pressed.
The values are always in UTC.

.. warning::
    The internal clock of the logger cannot track daylight saving times nor time
    zones. Hence, for correct logging, use UTC values for date and time.

.. note::
    The logger internal clock section is not affected by the buttons :ref:`Upload <upload-btn>`
    nor by :ref:`Set all <setall-btn>`.
    To change the logger internal clock use the buttons in this section.

**Get from computer**
    This button loads the current UTC time from the computer.

**Get from logger**
    This button loads the current UTC time from the connected logger.

**Set clock**
    This button will upload the value to the internal clock of the logger, and all
    other configurations. The change is **persistent**.
    That is, this button is equivalent to the :ref:`Upload <upload-btn>` button.

    .. note::
        This set button is not like the set buttons in the other sections, because
        the changes are persistent and it uploads the configuration from all the
        sections to the logger's persistent storage.

**Sync with computer**
    This button will sync the logger's internal clock with the one of the
    current computer.
    The synchronization is persistent.
    This will also make persistent the configurations in all the sections.
    That is, this button synchronizes the clock and does the same as the
    :ref:`Upload <upload-btn>` button.

    .. note::
        To minimize delays in the synchronization use this button instead of the
        sequence: **Get from Computer** and **Upload clock**

Name builder page
-----------------

.. figure:: _static/namebuilder_overview.png
    :alt: Name builder page

    The names builder is used to get names that follow the `Naming convention`_
    of the WABEsense project.

This is the name builder page of the UI.
The page is segmented in different sections:

    - `Location`_
    - `WABE`_
    - `Sensor`_

which you can recognize from the graphical representation of the UI.
The name builder page is a single `HTML form`_ that need to be filled to build names
that follow the `Naming convention`_ of the WABEsense project.

The **Submit** button will set the names in the `Configuration page`_ using the data in the form.
If the data is not correct, errors will be shown. They should help you to fill in correct data.
If all data is correct, you will be redirected to the `Configuration page`_.

Location
^^^^^^^^^
Information about the location of the logger


WABE
^^^^^^^^^
Information about the WABE to which the logged sensors are connected.

Sensor
^^^^^^^^^
Information about the sensors and their connections.

The default arrangement puts pressure on ADC_IN8 and temperature on ADC_IN09.
This is the convention of the project for the default logger sensor configuration.
However these values can change depending on the logger assembly.
Do check your logger to find to which ADC inputs are the sensors connected.

Working without a logger
------------------------
If you choose to **Continue without a logger** in the `Connection page`_,
all buttons that upload/download data to/from the logger are not available in
the `Configuration page`_.

.. figure:: _static/htmlui_nologger_overview.png
    :alt: Overview of the configuration page without connection to a logger

    Configuration page without connection to a logger, upload/download buttons
    are not available.

This is due to the fact that no logger is connected to the UI, hence data
cannot be sent to, nor retrieved from, the logger.
You can use this page to fill and save configurations to files for later use.


Links
-----
.. _Flask: https://flask.palletsprojects.com/en/1.1.x/
.. _Naming convention: https://hsr-iet.gitlab.io/wabesense/wabesense/installation.html#naming-convention
.. _issue tracker: https://gitlab.com/hsr-iet/wabesense/dataloggerui/-/issues
.. _HTML form: https://en.wikipedia.org/wiki/Form_(HTML)

.. target-notes::

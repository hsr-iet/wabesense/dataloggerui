===========
First steps
===========

If you are reading this document it is likely you have a
`WABEsense <https://hsr-iet.gitlab.io/wabesense/blog/>`_ data-logger
in your possession.
Here we describe the steps you should follow to get everything working properly.

Connecting the data-logger
--------------------------

Connect the data-logger over the USB port with the Laptop/PC.
On the Laptop/PC the logger is shown up as a virtual COM-Port.
For example on a linux system, after connecting the device the output of
``dmesg`` could look like::

    usb 1-3: new full-speed USB device number 2 using xhci_hcd
    usb 1-3: New USB device found, idVendor=0483, idProduct=5740, bcdDevice= 2.00
    usb 1-3: New USB device strings: Mfr=1, Product=2, SerialNumber=3
    usb 1-3: Product: STM32 Virtual ComPort
    usb 1-3: Manufacturer: STMicroelectronics
    usb 1-3: SerialNumber: 206935B04253
    cdc_acm 1-3:1.0: ttyACM0: USB ACM device
    usbcore: registered new interface driver cdc_acm
    cdc_acm: USB Abstract Control Model driver for USB modems and ISDN adapters

the specific USB port (here ``1-3``) might be different.

It should also be shown in the list of connected USB devices shown by the command
``lsusb``::

    Bus 001 Device 002: ID 0483:5740 STMicroelectronics Virtual COM Port

where the ``Bus``, ``Device`` and ``ID`` values will vary on different PC-logger
combinations.

import pandas as pd
from pathlib import Path

raw_names = dict (pressure='DTxx_pressure(V)', temperature='DTxx_temp(V)')

height_values = list(range(60, -10, -10))

if __name__ == '__main__':

  for fname in Path('/media/juanpi/9016-4EF8/').glob('*.csv'):
    if not fname.stem.startswith('SN'):
      print(f'Reading data from {fname.stem.split("_")[-1]}')

      df = pd.read_csv(fname, sep=';')
      df = df.iloc[-len(height_values):,:]
      df['height [cm]'] = height_values
      df['pressure [mbar]'] = df[raw_names['pressure']] * 100 / 5.0
      df['temperature [C]'] = df[raw_names['temperature']] * (50 + 40) / 5.0 - 40

      print(df[['height [cm]', 'pressure [mbar]', 'temperature [C]']].round(1))
      
      fout = Path().cwd() / fname.name.split('_')[-1]
      print(f'Saving to {fout.name} ...')
      df.to_csv(fout, sep=';')

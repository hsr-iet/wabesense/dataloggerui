import sys
from datetime import datetime as dt
import serial
from time import sleep
from itertools import chain
import re

control_chars = ''.join(map(chr, chain(range(0x00,0x20), range(0x7f,0xa0))))
control_char_re = re.compile('[%s]' % re.escape(control_chars))


def remove_control_chars(s):
    return control_char_re.sub('', s)


def hexlen_str(bstring):
  hlen = hex(len(bstring))[2:]
  hlen = '0' + hlen if len(hlen) == 1 else hlen
  return bytes.fromhex(hlen)


if __name__ == '__main__':
  port = '/dev/' + sys.argv[1]
  location = sys.argv[2]
  sensorID = sys.argv[3]
  dlogger = serial.Serial(port, timeout=0)

  blocation = location.encode('ascii')
  now_bstr = dt.utcnow().strftime('%Y.%m.%d %w %H:%M:%S').encode('ascii')
  bpress = f'0,{sensorID}_pressure(V)'.encode('ascii')
  btemp = f'1,{sensorID}_temp(V)'.encode('ascii')
  cmd = dict(
            locaton=b'\x02DNS' + hexlen_str(blocation) + blocation + b'\x03',
            ch8=b'\x02ANS' + hexlen_str(bpress) + bpress + b'\x03',
            ch9=b'\x02ANS' + hexlen_str(btemp) + btemp + b'\x03',
            time=b'\x02RTS' + hexlen_str(now_bstr) + now_bstr + b'\x03',
            save=b'\x02CSA\x00\x03'
        )
  for k, c in cmd.items():
    print(f'Sending {k}:{c} ...')
    count = dlogger.write(c)
    sleep(1)
    answ = dlogger.read(100).decode("ascii")
    if 'NACK' not in answ:
      print('Command acknowledged!')
    else:
      print('Something went wrong!')
      print(asnw)

import dataloggerui.serialui as s
import pandas as pd
import matplotlib.pyplot as plt
import sys
import scipy.stats as scs
import numpy as np

def get_logger_data(*, from_sample=0, resample=True, verbose=False):

    if not s.logger_present(): 
        s.connect_logger()

    n_total = s.get_sample_count()[1]

    if verbose:
        # setup toolbar
        print(f'Getting {n_total - from_sample - 1} samples:')

    df = []
    for n in range(from_sample, n_total):
        df.append(s.get_sample(sample=n)._asdict())
        if verbose:
            sys.stdout.write("\b" * 5)
            sys.stdout.write(f"{n:05d}")
            sys.stdout.flush()

    df = pd.DataFrame.from_records(df).set_index('datetime')
    sampling = s.get_sampling_interval()
    s_str = f'{sampling[0]:d}{sampling[1]}'

    if resample:
        df = df.rolling(s_str).mean().resample(s_str).mean()

    return df

if 'df' not in locals():
  df = get_logger_data(verbose=True)

axs = df[df.columns[df.columns.str.startswith('bme')]].plot(subplots=True, grid=True)
mng = plt.get_current_fig_manager()
mng.resize(*mng.window.maxsize())
plt.gcf().tight_layout()
every_hs = 12
t = [xx for xx in axs[0].get_lines()[0].get_xdata() if xx.hour % every_hs == 0]
tl = [u.strftime("%H:%M\n%d/%m") for u in t]
[(ax.set_xticks(t), ax.set_xticklabels(tl)) for ax in axs]
plt.xlabel('datetime UTC')


fig, ax = plt.subplots()
idx0 = df.index[df.bme_humidity < 25.2][0]
idx1 = df.index[df.bme_humidity < 30][-1]
x = (df.loc[idx0:idx1].index - idx0).total_seconds() / 60 / 1440
fit = scs.linregress(x, df.bme_humidity[idx0:idx1].to_numpy())
linf = f'{fit.slope:.2f} [%/day] t + {fit.intercept:.1f} [%]'
df.loc[idx0:idx1, linf] = fit.slope * x + fit.intercept

ax = df.loc[idx0:idx1, ['bme_humidity', linf]].plot(ax=ax, grid=True)
mng = plt.get_current_fig_manager()
mng.resize(*mng.window.maxsize())
plt.gcf().tight_layout()

every_hs = 12
t = [xx for xx in ax.get_lines()[0].get_xdata() if xx.hour % every_hs == 0]
ax.set_xticks(t)
tl = [u.strftime("%H:%M\n%d/%m") for u in t]
ax.set_xticklabels(tl)
plt.xlabel('datetime UTC')

plt.show()



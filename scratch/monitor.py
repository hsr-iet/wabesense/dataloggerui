import sys
import serial
import time
#import

if __name__ == '__main__':
    #port = '/dev/' + sys.argv[1]
    port = sys.argv[1]
    dlogger = serial.Serial(port, timeout=0)

    duration = int(sys.argv[2]) # duration in seconds

    time0 = time.time()
    while True:
        elapsed = time.time() - time0
        if elapsed > duration:
            break
       #dlogger.write(b'\x02MMS\x00\x03')
        dlogger.write(b'\x02RTG\x00\x03')
        time.sleep(0.5)
        answr = dlogger.read(100)
        print("{:05.3f}".format(elapsed), answr)

    dlogger.close()

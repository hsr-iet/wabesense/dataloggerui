=============================================
User interfaces for the WABEsense data-logger
=============================================

Python module to interact with the WABEsense data-logger.

Installation
------------

To install run::

    python -m pip install https://gitlab.com/hsr-iet/wabesense/dataloggerui/-/archive/master/dataloggerui-master.zip

This will install the development version of the module. We will offer releases soon.

You can also use git::

    python -m pip install git+https://gitlab.com/hsr-iet/wabesense/dataloggerui.git@master


To check that the installation went well you can run the following command in
a terminal::

    python -c 'import dataloggerui as ui; ui.describe()'


That program will print a bunch of information about the module.
If it works, then you are ready to use it.

For development
***************

To install all dependencies for development use the ``requirements.txt`` file::

    python -m pip install -U requirements.txt


Documentation
-------------

The documentation is hosted at https://hsr-iet.gitlab.io/wabesense/dataloggerui

To build the documentation you need `Sphinx <https://www.sphinx-doc.org/en/master/>`_.
Make sure you have all the dependencies, see section `For development`_ in the
installation instructions.
Once you have all the dependencies, go to the folder ``doc`` and run::

    make html

The documentation is then available inside ``doc/build/html``. To read it, open
the ``index.html`` file in that folder with your favorite internet browser.

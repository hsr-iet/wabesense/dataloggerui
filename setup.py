#!/usr/bin/env python3

""" setup script for data-logger UI module """

# Copyright (C) 2021 OST Ostschweizer Fachhochschule
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

from pathlib import Path
from setuptools import (
    setup,
    find_packages
)

__version__ = "1.0.1+dev"
__author__ = "Juan Pablo Carbajal"
__maintainer__ = ["Juan Pablo Carbajal"]
__email__ = "ajuanpi+dev@gmail.com"
__license__ = """
This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>."""

with open('README.rst', encoding='utf-8') as f:
    long_description = f.read()

script_files = Path()
setup(
    name="dataloggerui",
    packages=find_packages(),
    version=__version__,
    install_requires=["numpy",
                      "scipy",
                      "pandas",
                      "matplotlib",
                      "pyaml",
                      "pyserial",
                      "flask"],
    author=__author__,
    author_email=__email__,
    url="https://gitlab.com/hsr-iet/wabesense/dataloggerui/",
    description="Utilities to interacting with the WABEsense datalogger",
    long_description=long_description,
    long_description_content_type='text/x-rst',
    license=__license__,
    include_package_data=True,
    scripts=[str(f) for f in Path('scripts').glob('wsdl_*.py')],
)
